<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Disable automatic update
 *
 * @link http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */
define('AUTOMATIC_UPDATER_DISABLED', true);

/**
 * Limit number of revisions saved
 *
 * @link https://codex.wordpress.org/Revisions
 */
define('WP_POST_REVISIONS', 10);

/**
 * Update default mem limit
 *
 * @link http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP
 */
define('WP_MEMORY_LIMIT', '64M');

/**
 * Disable dashboard file editing
 *
 * @link http://codex.wordpress.org/Hardening_WordPress#Disable_File_Editing
 */
define('DISALLOW_FILE_EDIT', true);

/**
 * WordPress Environment
 *
 * The default usage is:
 * 	development - For local development
 *	production - For live site
 */
define('WP_ENV', 'development');

// Switch MySQL settings based on environment
switch(WP_ENV){
	// Development
	case 'development':
		/** The name of the local database for WordPress */
		define('DB_NAME', 'greenville_db');

		/** MySQL local database username */
		define('DB_USER', 'root');

		/** MySQL local database password */
		define('DB_PASSWORD', '');
	break;
	// Production
	case 'production':
		/** The name of the live database for WordPress */
		define('DB_NAME', 'live_database_name_here');

		/** MySQL live database username */
		define('DB_USER', 'live_username_here');

		/** MySQL live database password */
		define('DB_PASSWORD', 'live_password_here');
	break;
}

// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Custom Content Directory **/
define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/app');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '_!-7},3**#||D#$4~?i66++fM3q1Bvc2 S7+P*!Xk4e<Gl%r]#zep4oy1:jue$))');
 define('SECURE_AUTH_KEY',  '-#(JL|-ek+D)L~?P%YZ^WPUrt2Z&3IH`J0%e5rGVz+ Z&FYoEnd|)FVoeyK@|m^-');
 define('LOGGED_IN_KEY',    'p?&[{qzTKm-94UUzY-R?-yhywW;*]5pg`JyP^!.LrYm^>J5l-3q7W=P2F6GGvH!|');
 define('NONCE_KEY',        'H<e.n%-@/Qaa,^1TrBO~+XwRuXkDHzLbHEI[aq99Fqr]`zjXEwJ-}kT?Qf;9o@eH');
 define('AUTH_SALT',        '[oITi.,|v0%_UMT$UjlQuP]d%CNv0d>]u}{7 Xp>.Ph$-4}i|*4OAm-L|sW10^a$');
 define('SECURE_AUTH_SALT', 'O1c(nyc0mp1*<a@uL40cYma|-iz>]8au(_TV4I6-hRBT2XuQUpi+Gf4qS+RZF@yi');
 define('LOGGED_IN_SALT',   'L4/~3!|r4HeIQf5(O}n;+5p#v8v!B!qqcPqz@1iI?8[{8-J:]Lu#We={Anp1`A}&');
 define('NONCE_SALT',       'd7AfDJ<Nj@Zk-OHo|P+ZB[F3A_!e,<gA|,6O+BedtBHH0PBFTn.ycf$:TCZyik#j');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Enable theme debug mode **/
define('THEME_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/wp/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
