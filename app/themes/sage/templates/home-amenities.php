<?php
//Set Variables
$amenities_content = get_field('amenities_content_field', 2, true);
?>
<div class="c-amenities">
    <div class="o-row">
        <div class="o-col o-col--12 o-col--6@md c-amenities__left">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <?php echo $amenities_content ;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="o-col o-col--12 o-col--6@md c-amenities__right">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner c-amenities__image--right"
                    data-mobile="<?= get_field('right_image_amenities')['sizes']['w300x250'];?>" data-desktop="<?= get_field('right_image_amenities')['sizes']['w650x400'];?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- o-row -->
