<?php
//Set Variables
$blurb = get_field('blurb_field', 2, true);
?>
<div class="o-content c-blurb">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-blurb__content">
                <?php echo $blurb;?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
