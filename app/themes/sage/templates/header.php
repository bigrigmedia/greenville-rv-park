<?php
//Variables
$facebook = get_field('facebook_field', 2, true);
$twitter = get_field('twitter_field', 2, true);
$yelp = get_field('yelp_field', 2, true);
$advisor = get_field('advisor_field', 2, true);
?>

<header class="c-header">
    <button class="c-header__toggle c-header__toggle--htx"><span></span></button>
    <div class="c-header__mobile">
        <nav class="c-header__menu">
            <?php
            if (has_nav_menu('mobile_navigation')) :
                wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'c-nav__list']);
            endif;?>
        </nav>
    </div>
    <div class="o-container c-header__quicklinks">
        <div class="o-row">
            <div class="o-col o-col--6@md">
                <img src='/app/themes/sage/dist/images/telephone.png' class="c-header_icon">
                <div class="c-header__contact">
                    <ul>
                        <?php if ($facebook) { ?>
                            <li>
                                <a href="<?php echo $facebook; ?>">
                                    <img src='/app/themes/sage/dist/images/facebook.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($twitter) { ?>
                            <li>
                                <a href="<?php echo $twitter; ?>">
                                    <img src='/app/themes/sage/dist/images/twitter.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($yelp) { ?>
                            <li>
                                <a href="<?php echo $yelp; ?>">
                                    <img src='/app/themes/sage/dist/images/yelp.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($advisor) { ?>
                            <li>
                                <a href="<?php echo $advisor; ?>">
                                    <img src='/app/themes/sage/dist/images/advisor.png'>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="o-col o-col--6@md">
                <div class="c-header__contact">
                    <a class="c-btn c-btn--red" href="/reservations">RESERVE NOW</a>
                </div>
            </div>
            </div>
            <div class="o-row">
                <div class="o-col o-col--4@md c-header__nav--left">
                    <nav class="c-nav">
                        <?php
                        if (has_nav_menu('primary_navigation')) :
                            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'c-nav__list']);
                        endif;
                        ?>
                    </nav>
                </div>
                <div class="o-col o-col--4@md">
                    <a  href="<?= esc_url(home_url('/')); ?>"><img class="c-logo" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt=""/></a>
                </div>
                <div class="o-col o-col--4@md c-header__nav--right">
                    <nav class="c-nav">
                        <?php
                        if (has_nav_menu('secondary_navigation')) :
                            wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'c-nav__list']);
                        endif;
                        ?>
                    </nav>
                </div>
            </div>
        </div>
</header>
