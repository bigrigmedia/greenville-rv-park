<?php
//Set Variables
$gallery_content = get_field('gallery_content_field', 2, true);
?>
<div class="c-gallery">
    <div class="o-row">
        <div class="o-col o-col--12 o-col--6@md c-gallery__left">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <?php echo $gallery_content ;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="o-col o-col--12 o-col--6@md c-gallery__right">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner c-gallery__image--right"
                    data-mobile="<?= get_field('right_image_field')['sizes']['w300x250'];?>" data-desktop="<?= get_field('right_image_field')['sizes']['w650x400'];?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- o-row -->
