<div class="o-content c-testimonials" data-mobile="<?= get_field('image_background_field')['sizes']['w1920x720'];?>" data-desktop="<?= get_field('image_background_field')['sizes']['w1920x720'];?>">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-testimonials__content">
                <h2>Recent Reviews</h2>
                <?php echo do_shortcode('[testimonials]'); ?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
