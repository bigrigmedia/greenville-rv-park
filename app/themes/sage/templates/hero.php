
<?php if ( !is_front_page() && !is_home() ) {?>
    <div class="c-content__hero">
        <div class="c-hero">
            <?php echo do_shortcode( ' [soliloquy id=104] ' );?>
        </div>
    </div>
<?php }
    elseif (function_exists('soliloquy') && get_field('hero_slider')) {?>
        <div class="c-content__hero">
            <div class="c-hero">
                <?= do_shortcode('[soliloquy id="'.get_field('hero_slider')->ID.'"]'); ?>
            </div>
    <?php } ?>
        </div>
