<?php
//Set Variables
$contact = get_field('footer_contact_field', 2, true);
$facebook = get_field('facebook_field', 2, true);
$twitter = get_field('twitter_field', 2, true);
$yelp = get_field('yelp_field', 2, true);
$advisor = get_field('advisor_field', 2, true);
?>
<footer class="c-footer">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--2@xs c-footer__menu">
                <h5>QUICK LINKS</h5>
                <nav class="c-footer__menu">
                    <?php
                    if (has_nav_menu('footer_navigation')) :
                        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'c-nav__list']);
                    endif;?>
                </nav>
            </div>
            <div class="o-col o-col--4@xs c-footer__contact">
                <h5>Grenville Park Camping & RV Park</h5>
                <?php echo $contact ;?>
                <div class="c-footer__social">
                    <ul>
                        <?php if ($facebook) { ?>
                            <li>
                                <a href="<?php echo $facebook; ?>">
                                    <img src='/app/themes/sage/dist/images/facebookw.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($twitter) { ?>
                            <li>
                                <a href="<?php echo $twitter; ?>">
                                    <img src='/app/themes/sage/dist/images/twitterw.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($yelp) { ?>
                            <li>
                                <a href="<?php echo $yelp; ?>">
                                    <img src='/app/themes/sage/dist/images/yelpw.png'>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($advisor) { ?>
                            <li>
                                <a href="<?php echo $advisor; ?>">
                                    <img src='/app/themes/sage/dist/images/advisorw.png'>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="o-col o-col--6@xs c-footer__iframe">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2833.506784913559!2d-75.45397278446109!3d44.75007827909906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ccdac7331a0eba9%3A0xa362b500b0d7c4be!2sGrenville+Park!5e0!3m2!1sen!2sus!4v1491591672005" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>            </div>
        </div>
    </div>
    <div class="c-footer__rights">
        <div class="o-container">
            <p>©2017 grenville park camping & rv park | Website by <a href="http://www.bigrigmedia.com/">BIG RIG MEDIA LLC ®</a></p>
        </div>
    </div>
</footer>
