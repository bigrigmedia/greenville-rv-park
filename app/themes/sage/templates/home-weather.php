
<div class="o-content c-weather">
    <img src="/app/uploads/2017/04/ribbon.png" class="c-weather__ribbon">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--2@xs c-weather__content">
            </div>
            <div class="o-col o-col--8@xs c-weather__content">
                <?php echo do_shortcode('[wunderground location=14750 Rogers Rd, Patterson, CA 95363 numdays=4 layout=simple]');?>
            </div>
            <div class="o-col o-col--2@xs c-weather__content">
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
