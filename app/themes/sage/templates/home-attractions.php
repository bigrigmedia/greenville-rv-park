<?php
//Set Variables
$attractions_content = get_field('attractions_content_field', 2, true);
?>
<div class="c-attractions">
    <div class="o-row">
        <div class="o-col o-col--12 o-col--6@md c-attractions__image--left" data-mobile="<?= get_field('left_image_field')['sizes']['w320x300'];?>" data-desktop="<?= get_field('left_image_field')['sizes']['w960x420'];?>">
        </div>
        <div class="o-col o-col--12 o-col--6@md c-attractions__right">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <?php echo $attractions_content ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- o-row -->
