<?php
/**
 * Template Name: Home
 */
get_template_part('templates/home-blurb');
get_template_part('templates/home-gallery');
get_template_part('templates/home-attractions');
get_template_part('templates/home-amenities');
get_template_part('templates/home-testimonials');
