(function($) {
	// Testimonials
    if ($('.c-carousel--testimonials').length) {
        $('.c-carousel--testimonials').owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            navText: ['<', '>'],
            dots: true,
            autoplay: true,
            autoplayTimeout: 7000,
            autoplayHoverPause: true,
            autoplaySpeed: 1000,
            autoHeight: true
        });
    }
})(jQuery);
